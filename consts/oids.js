const oidSystem= [
    {
        name:'sysDescr',
        oid:'1.3.6.1.2.1.1.1.0'
    },
    {
        name:'sysName',
        oid:'1.3.6.1.2.1.1.5.0'
    },
    {
        name:'sysLocation',
        oid:'1.3.6.1.2.1.1.6.0'
    },
    {
        name:'sysContact',
        oid:'1.3.6.1.2.1.1.4.0'
    }
]

const oidIfs= [
    {
        name:'ifDescr',
        oid:'1.3.6.1.2.1.2.2.1.2'
    },
    {
        name:'ifAdminStatus',
        oid:'1.3.6.1.2.1.2.2.1.7'
    },
    {
        name:'ifOperStatus',
        oid:'1.3.6.1.2.1.2.2.1.8'
    },
    {
        name:'ifSpeed',
        oid:'1.3.6.1.2.1.2.2.1.5'
    },
    {
        name:'ifPhysAddress',
        oid:'1.3.6.1.2.1.2.2.1.6'
    },
    {
        name:'ifType',
        oid:'1.3.6.1.2.1.2.2.1.3'
    },
    {
        name:'ifNumber',
        oid:'1.3.6.1.2.1.2.1'
    },
]

module.exports = {
    oidSystem,
    oidIfs
}