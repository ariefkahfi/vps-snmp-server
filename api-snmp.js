const uniqid = require('uniqid')
const {sequelize} = require('./models/db')
const fs = require('fs')
const {execSync} = require('child_process')
const {DeviceModel} = require('./models/device')
const tStamp = require('unix-timestamp')
const {getInterfaceByName} =require('./models/interfaces')
sequelize.sync()


const systemRouter = require('./routes/apisystem')
const interfacesRouter = require('./routes/apiinf')
const express = require('express')
const app = express()   

const httpServer = require('http').createServer(app)



// view engine
app.set('view engine', 'pug')


// rrd files
app.use('/rrd',express.static('rrd'))
// rrd files

// static files
app.use('/assets/css',
    express.static(`${__dirname}/node_modules/bootstrap/dist/css`),
    express.static(`${__dirname}/assets/css`)
)

app.use('/assets/js',
    express.static(`${__dirname}/node_modules/bootstrap/dist/js`),
    express.static(`${__dirname}/node_modules/jquery/dist`),
    express.static(`${__dirname}/assets/js`)
)
// static files



// 

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// check system MIB
app.use('/system',systemRouter)

// check interfaces MIB
app.use('/interfaces',interfacesRouter)




// api controllers
app.post('/api/rrd/update',(req,res)=>{
    const {
        hostname,
        community,
        interfaceName,
        oidInterface
    } = req.body

    let hostnameDirPath = `${__dirname}/rrd/${hostname}`
    let interfaceNameDirPath = `${hostnameDirPath}/${interfaceName}`
    let rrdFilePath = `${interfaceNameDirPath}/ifIn.rrd`

    let isHostnameDirExists = fs.existsSync(hostnameDirPath)
    if(!isHostnameDirExists){
        fs.mkdirSync(hostnameDirPath)
    }
    let isInterfaceDirExists = fs.existsSync(interfaceNameDirPath)
    if(!isInterfaceDirExists){
        fs.mkdirSync(interfaceNameDirPath)
    }

    let isRrdFileExists = fs.existsSync(rrdFilePath)
    if(!isRrdFileExists){
        execSync(`rrdtool create "${rrdFilePath}" \
            --start N \
            --step 5 \
            DS:ifIn:COUNTER:10:U:U \
            RRA:AVERAGE:0.5:1:17280 \
        `)

        res.json({
            data:'create new rrd file',
            code:200
        })
    }else{
        try {
            let result = execSync(`snmpget -v1 -c${community} ${hostname} ${oidInterface}`)
            let lastIndexOfDdot = result.toString().lastIndexOf(':')
            let resultToString = result.toString()            
            
            let parsedVal = resultToString.substr( lastIndexOfDdot + 1, resultToString.length)
            let trimParsedVal = parsedVal.trim()

            execSync(`rrdtool update "${rrdFilePath}" ${tStamp.now()}:${trimParsedVal}`)

            res.json({
                code:200,
                data:'Rrd updated'
            })

        }catch(ex){
            console.error('ex',ex)
            res.json({
                code:200,
                data:'CANNOT_GET_THIS_VALUE'
            })
        }
    }
})


app.post('/api/rrd/graph',(req,res)=>{
    const {
        hostname,
        interfaceName
    } = req.body
    try {
        execSync(`rrdtool \
            graph "${__dirname}/rrd/${hostname}/${interfaceName}/ifIn.png" \
            --start now-3600s --end now \
            DEF:dIfIn="${__dirname}/rrd/${hostname}/${interfaceName}/ifIn.rrd":ifIn:AVERAGE \
            LINE1:dIfIn#FF0000:"If In ${interfaceName}"
        `)
        res.json({
            code:200,
            data:`rrd/${hostname}/${interfaceName}/ifIn.png`
        })
    }catch(ex){
        console.log('ex',ex)
        res.json({
            code:200,
            data:'CANNOT_GENERATE_GRAPH'
        })
    }
})

app.post('/api/device/:target/:comm/interfaceByName',(req,res)=>{
    getInterfaceByName(req.params.target , req.params.comm , req.body.interfaceName)
        .then(v=>{
            res.json({
                code:200,
                data:v
            })
        })
        .catch(err=>{
            console.error(err)
            res.status(500).json({
                code:500,
                data:'ERR_INTERNAL_SERVER'
            })
        })
})
app.get('/api/device/:device_hostname',(req,res)=>{
    DeviceModel.findOne({
        where:{
            device_hostname:req.params.device_hostname
        }
    })
        .then(r=>{
            res.json({
                code:200,
                data:r
            })
        })
        .catch(err=>{
            res.status(500).json({
                code:500,
                data:'ERR_INTERNAL_SERVER'
            })
        })
})
app.get('/api/device',(req,res)=>{
    DeviceModel.findAll()
        .then(r=>{
            res.json({
                code:200,
                data:r
            })
        })
        .catch(err=>{
            res.status(500).json({
                code:500,
                data:'ERR_INTERNAL_SERVER'
            })
        })
})
// api controllers


// front-end controllers
app.get('/',(req,res)=>{
    res.render('index',{
        pageTitle:'Index Page'
    })
})


app.get('/addDevice',(req,res)=>{
    res.render('device/add',{
        pageTitle:'Add device'
    })
})

app.post('/addDevice',(req,res)=>{
    DeviceModel.create({
        device_id:uniqid(),
        ...req.body
    })
        .then(r=>{
            res.redirect('/addDevice')
        })
        .catch(err=>{
            console.error(err)
            res.redirect('/addDevice')
        })
})

app.get('/listDevice',(req,res)=>{
    res.render('device/list',{
        pageTitle:'List device'
    })
})

app.get('/detailSystemDevice/:device_hostname',(req,res)=>{
    res.render('device/detailSystem',{
        pageTitle:'System detail'
    })
})
app.get('/detailInterfaceDevice',(req,res)=>{
    res.render('device/detailInterface',{
        pageTitle:'Interfaces'
    })
})
// front-end controllers



httpServer.listen(9602 , ()=>{
    console.log('listening on port 9602')
})



