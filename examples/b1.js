const {calculateInOut} = require('../models/promiseifOctet')


calculateInOut(
    '1.3.6.1.2.1.2.2.1.10.1',
    '1.3.6.1.2.1.2.2.1.16.1',
    'localhost',
    'public',
    '1.3.6.1.2.1.2.2.1.5.1'
)
    .then(n=>{
        console.log('bandwidth =',n)
    })
    .catch(err=>{
        console.error(err)
    })