
function loadSysName(target , community) {
    return fetch('/system/sysName',{
        method:'post',
        headers:{
            'content-type':'application/json'
        },
        body:JSON.stringify({
            target,
            community
        })
    })
}

function loadSysDescr(target , community) {
    return fetch(`/system/sysDescr`,{
        method:'post',
        headers:{
            'content-type':'application/json'
        },
        body:JSON.stringify({
            target,
            community
        })
    })
}


function loadSystem(){
    const pathName = window.location.pathname
    const splitPath = pathName.split('/')
    let apiResponse = null
    const lastPath = splitPath[splitPath.length - 1]
    
    fetch(`/api/device/${lastPath}`)
        .then(r=> r.json())    
        .then(rJson=>{
            return rJson.data
        })
        .then(json=>{
            apiResponse = json
        })
        .then(_=>{
            return loadSysName(apiResponse.device_hostname , apiResponse.community)
        })
        .then(response=>{
            return response.json()
        })
        .then(jsonSysName=>{
            $('#divSysNameContainerValue').text(jsonSysName.data)
            return loadSysDescr(apiResponse.device_hostname , apiResponse.community)
        })
        .then(response=>{
            return response.json()
        })
        .then(jsonSysDescr=>{
            $('#divSysDescrContainerValue').text(jsonSysDescr.data)
        })
        .catch(err=>{
            console.error(err)
        })
}


loadSystem()