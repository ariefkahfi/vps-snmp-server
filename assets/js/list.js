
function changeHrefAndText(idA, href) {
    $(idA).attr('href',href)
}

function onClickDetail(deviceHostname , community) {
    $('#modal-device').modal('show')

    changeHrefAndText('#aInterface',`/detailInterfaceDevice?target=${deviceHostname}&community=${community}`)
    changeHrefAndText('#aSystem',`/detailSystemDevice/${deviceHostname}`)
}


function loadTable() {
    fetch(`/api/device`)
        .then(r=> r.json())    
        .then(rJson=>{
            const {data} = rJson
            $('#table-body').empty()

            data.forEach(v=>{
                $('#table-body').append(`
                    <tr>
                        <td>${v.device_name}</td>
                        <td>${v.device_hostname}</td>
                        <td>${v.community}</td>
                        <td>
                            <a class="btn btn-primary text-light" onclick="onClickDetail('${v.device_hostname}' , '${v.community}')">Detail</a>
                        </td>
                    </tr>
                `)
            })
        })
        .catch(err=>{
            console.error(err)
        })
}

loadTable()