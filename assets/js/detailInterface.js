let urlParam = new URLSearchParams(window.location.search)
const target = urlParam.get('target')
const community = urlParam.get('community')
$('#loading-container').hide()


$('#modal-graph').on('hidden.bs.modal',e=>{
    console.log('on hidden.bs.modal')
    $('#modal-graph-img').attr('src','')
    clearInterval(timerUpdateRrd)
})

let timerUpdateRrd = null

function requestGraphRrd(body) {
    return fetch(`/api/rrd/graph`,{
        method:'POST',
        headers:{
            'content-type':'application/json'
        },
        body:JSON.stringify(body)
    })
}

function requestUpdateRrd(body){
    fetch(`/api/rrd/update`,{
        method:'POST',
        headers:{
            'content-type':'application/json'
        },
        body:JSON.stringify(body)
    })
        .then(r=> r.json())
        .then(rJson=>{
            console.log(`then updateRrd`,rJson)
        })
        .then(_=>{
            return requestGraphRrd({
                interfaceName:body.interfaceName,
                hostname:body.hostname
            })
        })
        .then(r=> r.json())
        .then(rJson=>{ 
            return fetch(`${window.location.protocol}//${window.location.host}/${rJson.data}`)
                .then(r=> r.blob())
        })
        .then(blobData=>{
            let blobUrl = window.URL.createObjectURL(blobData)
            $('#modal-graph-img').attr('src',blobUrl)
        })
        .catch(err=>{

        })
}



function onInterfaceDetail(ifDescr){
    $('#modal-graph').modal('show')

    fetch(`/api/device/${target}/${community}/interfaceByName`,{
        method:'POST',
        headers:{
            'content-type':'application/json'
        },
        body:JSON.stringify({
            interfaceName:ifDescr
        })
    })
        .then(v=>v.json())
        .then(vJson=>{
            return {
                ...vJson.data,
                target,
                community
            }
        })
        .then(passedData=>{
            let splitOid = passedData.oid.toString().split('.')
            let whichInterface = splitOid[splitOid.length - 1]
            
            timerUpdateRrd = setInterval(()=>{
                requestUpdateRrd({
                    hostname:passedData.target,
                    interfaceName:passedData.value,
                    oidInterface:`.1.3.6.1.2.1.2.2.1.10.${whichInterface}`,
                    community:passedData.community
                })
            },5000)
        })
        .then(_=>{

        })
        .catch(err=>{
        })
}

function loadInterfacesAndStatus(){
    $('#loading-container').show()
    console.log('show loading-container...')

    

    fetch('/interfaces/listInterfacesAndStatus',{
        method:'post',
        headers:{
            'content-type':"application/json"
        },
        body:JSON.stringify({target , community})
    })
        .then(r=> r.json())
        .then(rJson=>{
            const {data} = rJson


            
            if(data === 'ERR') {
                return    
            }


            $('#loading-container').hide()
            $('#card-container').empty()
            data.forEach(v=>{
                let isUp = ''
                let isPointer = 'unset'
                let isClickable = ''

                switch(parseInt(v.status)) {
                    case 1:
                    isUp = 'bg-success'
                    isPointer = 'pointer'
                    isClickable = `onInterfaceDetail('${v.descr}')`
                    break;
                    case 2:
                    isUp = 'bg-danger'
                    break;
                    default:
                    isUp = 'bg-secondary'
                    break;
                }


                $('#card-container').append(`
                    <div class="d-flex my-2 bg-light" onclick="${isClickable}" style="cursor: ${isPointer}">
                        <div class="${isUp} pr-2"></div>
                        <div class="flex-grow-1 p-2">${v.descr}</div> 
                    </div>
                `)
            })
        })
        .catch(err=>{
        })

}


loadInterfacesAndStatus()

let timerInterfaceAndStatus 
    = setInterval(()=>{
        loadInterfacesAndStatus()
    },5000)