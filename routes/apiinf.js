const router = require('express').Router()
const {oidIfs} = require('../consts/oids')
const {snmp} = require('../snmp/client')
const {listInterfaceAndItsStatus} = require('../models/interfaces')

const ifDescr = oidIfs[0]
const ifAdminStatus = oidIfs[1]
const ifOperStatus = oidIfs[2]
const ifSpeed = oidIfs[3]
const ifPhysAddress = oidIfs[4]
const ifType = oidIfs[5]
const ifNumber = oidIfs[6]




router.post(`/${ifDescr.name}`,(req,res)=>{
    let arrData = []
    const {
        target,
        community
    } = req.body

    const session = snmp.createSession(target , community)
    session.subtree(ifDescr.oid,varbinds=>{
        varbinds.forEach(v=>{
            if(snmp.isVarbindError(v)){
                console.error(snmp.varbindError(v))
                res.status(500).json({
                    code:500,
                    data:'Cannot get this value'
                })
                return
            }
            arrData.push(v.value.toString())
        })
    },err=>{
        session.close()
        if(err){
            console.error(err)
            return
        }
        console.log(`subtree done at ${req.url} :: ${req.method}`)
        res.json({
            code:200,
            data:arrData
        })
    })
})
router.post(`/${ifAdminStatus.name}`,(req,res)=>{
    let arrData = []
    const {
        target,
        community
    } = req.body

    const session = snmp.createSession(target , community)

    session.subtree(ifAdminStatus.oid,varbinds=>{
        varbinds.forEach(v=>{
            if(snmp.isVarbindError(v)){
                console.error(snmp.varbindError(v))
                res.status(500).json({
                    code:500,
                    data:'Cannot get this value'
                })
                return
            }
            let status = ''

            switch (parseInt(v.value.toString())) {
                case 1:
                    status = `up(${v.value.toString()})`
                break;
                case 2:
                    status = `down(${v.value.toString()})`
                break;
                case 3:
                    status = `testing(${v.value.toString()})`
                break;
            }
            arrData.push(status)
        })
    },err=>{
        session.close()
        if(err){
            console.error(err)
            return
        }
        console.log(`subtree done at ${req.url} :: ${req.method}`)
        res.json({
            code:200,
            data:arrData
        })
    })
})
router.post(`/${ifOperStatus.name}`,(req,res)=>{
    let arrData = []
    const {
        target,
        community
    } = req.body

    const session = snmp.createSession(target , community)

    session.subtree(ifOperStatus.oid,varbinds=>{
        varbinds.forEach(v=>{
            if(snmp.isVarbindError(v)){
                console.error(snmp.varbindError(v))
                res.status(500).json({
                    code:500,
                    data:'Cannot get this value'
                })
                return
            }

            let status = ''

            switch (parseInt(v.value.toString())) {
                case 1:
                    status = `up(${v.value.toString()})`
                break;
                case 2:
                    status = `down(${v.value.toString()})`
                break;
                case 3:
                    status = `testing(${v.value.toString()})`
                break;
                case 4:
                    status = `unknown(${v.value.toString()})`
                break;
                case 5:
                    status = `dormant(${v.value.toString()})`
                break;
                case 6:
                    status = `notPresent(${v.value.toString()})`
                break;
                case 7:
                    status = `lowerLayerDown(${v.value.toString()})`
                break;
            }

            arrData.push(status)
        })
    },err=>{
        session.close()
        if(err){
            console.error(err)
            return
        }
        console.log(`subtree done at ${req.url} :: ${req.method}`)
        res.json({
            code:200,
            data:arrData
        })
    })
})
router.post(`/${ifSpeed.name}`,(req,res)=>{
    let arrData = []
    const {
        target,
        community
    } = req.body

    const session = snmp.createSession(target , community)

    session.subtree(ifSpeed.oid,varbinds=>{
        varbinds.forEach(v=>{
            if(snmp.isVarbindError(v)){
                console.error(snmp.varbindError(v))
                res.status(500).json({
                    code:500,
                    data:'Cannot get this value'
                })
                return
            }
            arrData.push(v.value.toString())
        })
    },err=>{
        session.close()
        if(err){
            console.error(err)
            return
        }
        console.log(`subtree done at ${req.url} :: ${req.method}`)
        res.json({
            code:200,
            data:arrData
        })
    })
})

router.post('/listInterfacesAndStatus',(req,res)=>{
    const {
        target,
        community
    } = req.body

    listInterfaceAndItsStatus(target , community)
        .then(data=>{
            res.json({
                code:200,
                data
            })
        })
        .catch(err=>{
            console.error(err)
            res.status(500).json({
                code:500,
                data:'ERR'
            })
        })

})

router.post(`/${ifNumber.name}`,(req,res)=>{
    let arrData = []
    const {
        target,
        community
    } = req.body

    const session = snmp.createSession(target , community)

    session.subtree(ifNumber.oid,varbinds=>{
        varbinds.forEach(v=>{
            if(snmp.isVarbindError(v)){
                console.error(snmp.varbindError(v))
                res.status(500).json({
                    code:500,
                    data:'Cannot get this value'
                })
                return
            }
            arrData.push(v.value.toString())
        })
    },err=>{
        session.close()
        if(err){
            console.error(err)
            return
        }
        console.log(`subtree done at ${req.url} :: ${req.method}`)
        res.json({
            code:200,
            data:arrData
        })
    })
})




module.exports = router