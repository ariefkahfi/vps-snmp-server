const router = require('express').Router()
const {snmp} = require('../snmp/client')
const {oidSystem} = require('../consts/oids')

const sysDescr = oidSystem[0]
const sysName = oidSystem[1]
const sysContact = oidSystem[3]


router.post('/sysContact',(req,res)=>{
    const {
        target,
        community
    } = req.body

    const session = snmp.createSession(target , community)

    session.get([ sysContact.oid ],(err,varbinds)=>{
        if (err) {
            console.error(err)
            res.status(500).json({
                code:500,
                data:err.message
            })
            return
        }
        varbinds.forEach(v=>{
            console.log(`forEach varbinds`,v)
            if(snmp.isVarbindError(v)){
                res.status(500).json({
                    code:500,
                    data:null,
                })
                return
            }
            res.json({
                code:200,
                data:v.value.toString()
            })
        })

        session.close()
    })
})

router.post('/sysDescr',(req,res)=>{
    const {
        target,
        community
    } = req.body

    const session = snmp.createSession(target , community)

    session.get([ sysDescr.oid ],(err,varbinds)=>{
        if (err) {
            console.error(err)
            res.status(500).json({
                code:500,
                data:err.message
            })
            return
        }
        varbinds.forEach(v=>{
            console.log(`forEach varbinds`,v)
            if(snmp.isVarbindError(v)){
                res.status(500).json({
                    code:500,
                    data:null,
                })
                return
            }
            res.json({
                code:200,
                data:v.value.toString()
            })
        })

        session.close()
    })
})

router.post('/sysName',(req,res)=>{

    const {
        target,
        community
    } = req.body

    const session = snmp.createSession(target , community)

    session.get([sysName.oid] , (err,varbinds)=>{
        if(err){
            console.error(err)
            res.status(500).json({
                code:500,
                data:'Cannot get this value'
            })
            return
        }

        if (varbinds && varbinds.length > 0) {
            varbinds.forEach(v=>{
                if(snmp.isVarbindError(v)){
                    res.status(500).json({
                        code:500,
                        data:'Cannot get this value'
                    })
                    return
                }
                res.json({
                    code:200,
                    data:v.value.toString()
                })
            })
        }
        session.close()
    })
})

module.exports = router
