const snmp =  require('net-snmp')



function snmpGet(oidtoScan , target , community , onErr , onOk) {
    const session = snmp.createSession(target , community)

    session.get([oidtoScan],(err,varbinds)=>{
        if(err){
            onErr(err)
            return
        }
        onOk(varbinds)
    })
}


module.exports = {
    snmp,
    snmpGet
}