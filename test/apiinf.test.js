const snmp = require('net-snmp')
const assert = require('assert')
const session = snmp.createSession('localhost','public')
const {listInterfaceAndItsStatus , getInterfaceByName} = require('../models/interfaces')
const {inOut  , ifInOctet , ifOutOctet  } = require('../models/promiseifOctet')




describe('interfaces SNMP',_=>{

    describe.only('check interface by its name',_=>{
        it('must not null',done=>{
            getInterfaceByName('arka-it.xyz','public','Red Hat, Inc Device 0001')
                .then(v=>{
                    assert.notStrictEqual(v , null)
                    console.log(v)                    
                    done()
                })
                .catch(err=>{
                    done(err)
                })
        })
    })

    describe('check ifDescr and ifOperStatus',_=>{
        it('must not error',done=>{
            listInterfaceAndItsStatus('arka-it.xyz','public')
                .then(data=>{
                    console.log(data)
                    done()
                })
                .catch(err=>{
                    done(err)
                })
        }).timeout(600000)
    }).timeout(600000)

    describe('check in octet (Wireless network) interface on localhost',_=>{
        it('must not error',done=>{
            ifInOctet('1.3.6.1.2.1.2.2.1.10.3','localhost','public')
                .then(v=>{
                    console.log(parseInt(v.value.toString()))
                    done()
                })
                .catch(err=>{
                    done(err)
                })
        })
    })

    describe('check out octet (Wireless network) interface on localhost',_=>{ 
        it('must not erorr',done=>{
            ifOutOctet('1.3.6.1.2.1.2.2.1.16.3','localhost','public')
                .then(v=>{
                    console.log(parseInt(v.value.toString()))
                    done()
                })
                .catch(err=>{
                    done(err)
                })
        })
    })


    describe('check in out octet (Wireless network ) interface on localhost',_=>{
        it('must not error',done=>{
            inOut(
                '1.3.6.1.2.1.2.2.1.10.3',
                '1.3.6.1.2.1.2.2.1.16.3',
                'localhost',
                'public'
            )
                .then(v=>{
                    console.log(v)
                    done()
                })
                .catch(err=>{
                    done(err)
                })
        })
    })

    describe("check ifDescr ",_=>{
        let arrData = []
        it("must return list of ifDescr",done=>{
            session.subtree('1.3.6.1.2.1.2.2.1.2',varbinds=>{
                varbinds.forEach(v=>{
                    if(snmp.isVarbindError(v)){
                        done(snmp.varbindError(v))
                    }
                    arrData.push(v.value.toString())
                })
            },err=>{
                if(err){
                    done(err)
                    return
                }
                console.log(arrData)
                done()
            })            
        })
    })

}).timeout(600000)