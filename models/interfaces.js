const {snmp} = require('../snmp/client')

const {oidIfs} = require('../consts/oids')


const oidIfDescr = oidIfs[0]
const oidIfOperStatus = oidIfs[2]

function getInterfaceByName(target , community  , interfaceName){
    const session = snmp.createSession(target , community)
    let resolveValue = null
    return new Promise((resolve , reject)=>{
        session.subtree(oidIfDescr.oid , varbinds=>{
            varbinds.forEach(v=>{
                if(snmp.isVarbindError(v)){
                    reject(snmp.varbindError(v))
                }
                if(v.value.toString() === interfaceName){
                    resolveValue = {
                        oid:v.oid,
                        value:v.value.toString()
                    }
                }
            })
        },err=>{
            if(err){
                reject(err)
            }
            resolve(resolveValue)
        })
    })
}



function listInterfaceDescr(target , community) {
    const session = snmp.createSession(target , community)
    let arrValue = []

    return new Promise((resolve,reject)=>{
        session.subtree(oidIfDescr.oid,(varbinds)=>{
            varbinds.forEach(v=>{
                if(snmp.isVarbindError(v)){
                    reject(snmp.varbindError(v))
                }
                arrValue.push(v.value.toString())
            })
        },err=>{
            if(err){
                reject(err)
            }
            resolve(arrValue)
            session.close()
        })
    })
}

function listInterfaceOperStatus(target , community){
    const session = snmp.createSession(target , community)
    let arrValue = []

    return new Promise((resolve,reject)=>{
        session.subtree(oidIfOperStatus.oid,(varbinds)=>{
            varbinds.forEach(v=>{
                if(snmp.isVarbindError(v)){
                    reject(snmp.varbindError(v))
                }
                arrValue.push(v.value.toString())
            })
        },err=>{
            if(err){
                reject(err)
            }
            resolve(arrValue)
            session.close()
        })
    })
}

async function listInterfaceAndItsStatus(target , community) {
    const ifDescrList = await listInterfaceDescr(target , community)
    const ifOperStatusList = await listInterfaceOperStatus(target , community)

    let newArray = []

    for(let i = 0; i <ifDescrList.length; i++){
        newArray.push({
            descr:ifDescrList[i],
            status:ifOperStatusList[i]
        })
    }
    return newArray
}


module.exports = {
    listInterfaceAndItsStatus,
    getInterfaceByName
}