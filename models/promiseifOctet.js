const {snmp} = require('../snmp/client')
const dt = require('timediff')
const {sleep} = require("sleep")


function ifInOctet(oidInf, target ,community) { 
    const session = snmp.createSession(target , community)
    return new Promise((resolve,reject)=>{
        session.get([oidInf] , (err,varbinds)=>{
            if(err){
                reject(err)
                return
            }
            varbinds.forEach(v=>{
                if(snmp.isVarbindError(v)){
                    reject(snmp.varbindError(v))
                    return
                }
                resolve(v)
            })
            session.close()
        })
    })
}

function ifOutOctet(oidInf , target , community) {
    const session = snmp.createSession(target , community)
    return new Promise((resolve , reject)=>{
        session.get([oidInf] , (err,varbinds)=>{
            if(err){
                reject(err)
                return
            }
            varbinds.forEach(v=>{
                if(snmp.isVarbindError(v)){
                    reject(snmp.varbindError(v))
                    return
                }
                resolve(v)
            })
            session.close()
        })
    })
}

function ifSpeed(oid ,target , community) {
    const session = snmp.createSession(target , community)
    return new Promise((resolve , reject)=>{
        session.get([oid],(err,varbinds)=>{
            if(err){
                reject(err)
            }
            varbinds.forEach(v=>{
                if(snmp.isVarbindError(v)){
                    reject(snmp.varbindError(v))
                }
                resolve(v)
            })
        })
    })
}

async function calculateInOut(inOid , outOid , target, community , oidIfSpeed) {
    const d1IfInOctet = await ifInOctet(inOid , target , community)
    const d1IfOutOctet = await ifOutOctet(outOid , target , community)
    const t1 = Date.now()
    sleep(5)

    const d2IfInOctet = await ifInOctet(inOid , target, community)
    const d2IfOutOctet = await ifOutOctet(outOid , target , community)
    const t2 = Date.now()

    const deltaTime = dt(t1 , t2 , 'S')
    
    const deltaIfIn = parseInt(d2IfInOctet.value) - parseInt(d1IfInOctet.value)
    const deltaIfOut = parseInt(d2IfOutOctet.value) - parseInt(d1IfOutOctet.value)

    console.log(deltaIfIn , deltaIfOut)

    const vIfSpeed = await ifSpeed(oidIfSpeed , target , community)

    const up = (deltaIfIn + deltaIfOut) * 8 * 100
    const down = deltaTime.seconds * parseInt(vIfSpeed.value)

    console.log(up , down)  

    return (up / down)
}

async function inOut(inOid , outOid , target , community) {
    const inInf = await ifInOctet(inOid , target , community)
    const outInf = await ifOutOctet(outOid , target , community)
    return Promise.resolve({
        in:inInf,
        out:outInf
    })
}

module.exports = {
    ifInOctet ,
    ifOutOctet ,
    inOut , 
    calculateInOut
}