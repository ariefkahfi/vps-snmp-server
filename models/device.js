const {sequelize} = require('./db')


const DeviceModel = sequelize.define('device',{
    device_id:{
        type:sequelize.Sequelize.STRING,
        primaryKey:true
    },
    device_name:{
        type:sequelize.Sequelize.STRING,
        allowNull:false
    },
    device_hostname:{
        type:sequelize.Sequelize.STRING,
        unique:true,
        allowNull:false
    },
    community:{
        type:sequelize.Sequelize.STRING,
        allowNull:false
    }
},{
    timestamps:false,
    tableName:'device'
})


module.exports = {
    DeviceModel
}